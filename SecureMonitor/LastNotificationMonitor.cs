﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SecureMonitor
{
    public  class LastNotificationMonitor
    {
        private const int TEST_INTERVAL = 600000;
        private  DateTime _lastAlertTime;

        private const double NOTIFICATION_INTERVAL = 60; //Minutes
        private const double ALERT_INTERVAL = 30; //Minutes

        private  bool _lastCheckFailed = false;

        //  private static List<string> phoneNumberList = new List<string>(new string[] { "12392936387", "14434783962", "17034154545", "19253838376" });
        private static List<string> phoneNumberList;// = new List<string>(new string[] { "12392936387" });
        private static string conString = String.Empty; //"Server=tcp:sentinelsecuresql.database.windows.net,1433;Initial Catalog=SentinelSecure;Persist Security Info=False;User ID=ssuser;Password=SyncDog1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        //  private static readonly string conString = "Server=(localdb)\\MSSQLLocalDB;Database=SentinelSecure;User Id=sa;Password=SyncDog1;";

        private static string Environment = String.Empty;


        public LastNotificationMonitor(string connectionStr, string environment, List<string> notificationList)
        {
            conString = connectionStr;
            _lastAlertTime = DateTime.UtcNow.AddMinutes(-65);
            _lastCheckFailed = false;
            Environment = environment;
            phoneNumberList = notificationList;
        }


    
        public  void RunLastNotificationMonitor()
        {

            Console.WriteLine("Begin LastNotification test");

            using (SqlConnection con = new SqlConnection(conString))
            {
                var sqlText = "  SELECT MAX(CONVERT(datetime,LastNotificationEmailDate)) as LastNotification FROM dbo.DeviceEmailAccount";

                using (SqlCommand cmd = new SqlCommand(sqlText, con))
                {
                    con.Open();
                    var dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        DateTime lastNotification = (DateTime) dr["LastNotification"];

                        if (lastNotification < DateTime.UtcNow.AddMinutes(-NOTIFICATION_INTERVAL))
                        {
                            Console.WriteLine("LastNotification test failed");
                            _lastCheckFailed = true;

                            var now = DateTime.UtcNow;
                            var timeSpan = DateTime.UtcNow - lastNotification;

                            var easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            var easternTime = TimeZoneInfo.ConvertTimeFromUtc(lastNotification, easternZone);



                            if (_lastAlertTime < now.AddMinutes(-ALERT_INTERVAL))
                            {
                             
                                Console.WriteLine("ALERT: LastNotification: {0}", lastNotification);
                                var alert = new SmsAlert();


                                //var errorMsg = String.Format("Last Notification {0} EST ( more than {1} ago)", easternTime.ToString("MM/dd/yy HH:mm:ss"),timeSpan.ToString(@"hh\:mm\:ss"));

                                var errorMsg = String.Format("Alert: last notification more than {0} Days, {1} Hours, {2} Minutes ago",  timeSpan.ToString(@"dd"), timeSpan.ToString(@"hh"), timeSpan.ToString(@"ss"));
                                alert.SendSmsAlert(errorMsg, phoneNumberList);

                                DBLogger.LogResult(errorMsg, 1, true, 2);
                                DBLogger.UpdateCurrentStatus(1, 2, 2, lastNotification);

                                _lastAlertTime = DateTime.UtcNow;
                            } else
                            {
                                Console.WriteLine("LastNotification test failed no alert");
                                var errorMsg = String.Format("Last Notification {0} (more than {1} Days, {2} Hours, {3} Minutes ago)", easternTime.ToString("MM/dd/yy HH:mm:ss"), timeSpan.ToString(@"dd"), timeSpan.ToString(@"hh"), timeSpan.ToString(@"ss"));
                                DBLogger.LogResult(errorMsg, 1, false, 2);
                                DBLogger.UpdateCurrentStatus(1, 2, 2, lastNotification);
                            }

                           
                        }else
                        {
                            // Send all clear??
                            Console.WriteLine("LastNotification test successful");

                            if (_lastCheckFailed)
                            {
                                var alert = new SmsAlert();
                                var errorMsg = String.Format("Last Notification time all clear");

                                alert.SendSmsAlert(errorMsg, phoneNumberList);
                                DBLogger.LogResult(errorMsg, 1, true, 2);
                          
                                Console.WriteLine(errorMsg);
                            }
                            DBLogger.UpdateCurrentStatus(1, 2, 1, lastNotification);
                            _lastCheckFailed = false;
                        }
                       
                    }


                    cmd.Dispose();
                    con.Close();
                    con.Dispose();
                }

                
            }

            Console.WriteLine("End LastNotification test");
        }
    }
}
