﻿using SecureMonitor.Domain;
using System.Data.Entity;

namespace SecureMonitor
{
    public class SecureMonitorContext : DbContext
    {
        public SecureMonitorContext(): base("name=SecureMonitorConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SecureMonitorContext, Migrations.Configuration>());

        }

        public DbSet<SystemSetting> SystemSettings { get; set; }
        public DbSet<MonitorAgent> MonitorAgents { get; set; }
        public DbSet<MonitorResult> MonitorResults { get; set; }
        public DbSet<CurrentStatus> CurrentStatus { get;set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
