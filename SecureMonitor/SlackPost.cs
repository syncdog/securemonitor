﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace SecureMonitor
{
    class SlackPost
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string SlackUrl = "https://hooks.slack.com/services/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";


        private class SlackMessage
        {
            [JsonProperty("text")]
            public string Text { get; set; }
        }


        public static async void PostSlackMessage(string message)
        {
            var msg = new SlackMessage()
            {
                Text = message
            };

            var stringPayload = JsonConvert.SerializeObject(msg);

            // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

            var result = await client.PostAsync(SlackUrl, httpContent);

            Console.WriteLine(result);
        }
    }
}
