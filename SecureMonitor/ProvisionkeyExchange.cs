﻿using System.Collections.Generic;
using System.Security.Cryptography;

namespace SecureMonitor
{
    public class ProvisionKeyExchange
    {
        private CngKey deviceKey;
        private CngKey serverKey;
        private byte[] devicePubKeyBlob;
        private byte[] serverPubKeyBlob;

        //static void Main()
        //{
        //    CreateKeys();
        //    byte[] encrytpedData = deviceSendsData("secret message");
        //    ServerReceivesData(encrytpedData);
        //    Console.Read();
        //}

        public List<object> CreateECDHKey()
        {
            List<object> keyList = new List<object>();
            //serverKey = CngKey.Create(CngAlgorithm.ECDiffieHellmanP256);
            serverKey = CngKey.Create(CngAlgorithm.ECDiffieHellmanP256, null, new CngKeyCreationParameters { ExportPolicy = CngExportPolicies.AllowPlaintextExport });
            serverPubKeyBlob = serverKey.Export(CngKeyBlobFormat.EccPublicBlob);
            keyList.Add(serverKey);
            keyList.Add(serverPubKeyBlob);

            return keyList;
        }

        //private static void CreateKeys()
        //{
        //    deviceKey = CngKey.Create(CngAlgorithm.ECDiffieHellmanP256);
        //    serverKey = CngKey.Create(CngAlgorithm.ECDiffieHellmanP256);
        //    devicePubKeyBlob = deviceKey.Export(CngKeyBlobFormat.EccPublicBlob);
        //    serverPubKeyBlob = serverKey.Export(CngKeyBlobFormat.EccPublicBlob);
        //}

        //private static byte[] deviceSendsData(string message)
        //{
        //    Console.WriteLine("device sends message: {0}", message);
        //    byte[] rawData = Encoding.UTF8.GetBytes(message);
        //    byte[] encryptedData = null;

        //    using (var deviceAlgorithm = new ECDiffieHellmanCng(deviceKey))
        //    using (CngKey serverPubKey = CngKey.Import(serverPubKeyBlob,
        //         CngKeyBlobFormat.EccPublicBlob))
        //    {
        //        byte[] symmKey = deviceAlgorithm.DeriveKeyMaterial(serverPubKey);
        //        Console.WriteLine("device creates this symmetric key with " +
        //              "servers public key information: {0}",
        //              Convert.ToBase64String(symmKey));

        //        using (var aes = new AesCryptoServiceProvider())
        //        {
        //            aes.Key = symmKey;
        //            aes.GenerateIV();
        //            using (ICryptoTransform encryptor = aes.CreateEncryptor())
        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                // create CryptoStream and encrypt data to send 
        //                var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);

        //                // write initialization vector not encrypted 
        //                ms.Write(aes.IV, 0, aes.IV.Length);
        //                cs.Write(rawData, 0, rawData.Length);
        //                cs.Close();
        //                encryptedData = ms.ToArray();
        //            }
        //            aes.Clear();
        //        }
        //    }


        //    Console.WriteLine("device: message is encrypted: {0}",
        //           Convert.ToBase64String(encryptedData)); ;
        //    Console.WriteLine();
        //    return encryptedData;
        //}

        //private static void ServerReceivesData(byte[] encryptedData)
        //{
        //    Console.WriteLine("server receives encrypted data");
        //    byte[] rawData = null;

        //    var aes = new AesCryptoServiceProvider();
        //    int nBytes = aes.BlockSize >> 3;
        //    byte[] iv = new byte[nBytes];
        //    for (int i = 0; i < iv.Length; i++)
        //        iv[i] = encryptedData[i];

        //    using (var serverAlgorithm = new ECDiffieHellmanCng(serverKey))
        //    using (CngKey devicePubKey = CngKey.Import(devicePubKeyBlob,
        //          CngKeyBlobFormat.EccPublicBlob))
        //    {
        //        byte[] symmKey = serverAlgorithm.DeriveKeyMaterial(devicePubKey);
        //        Console.WriteLine("server creates this symmetric key with " +
        //              "devices public key information: {0}",
        //              Convert.ToBase64String(symmKey));

        //        aes.Key = symmKey;
        //        aes.IV = iv;

        //        using (ICryptoTransform decryptor = aes.CreateDecryptor())
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write);
        //            cs.Write(encryptedData, nBytes, encryptedData.Length - nBytes);
        //            cs.Close();
        //            rawData = ms.ToArray();

        //            Console.WriteLine("server decrypts message to: {0}",
        //                  Encoding.UTF8.GetString(rawData));
        //        }
        //        aes.Clear();
        //    }
        //}
    }
}
