﻿using System.Text.RegularExpressions;

namespace SecureMonitor
{
    public static class PhoneHelper
    {
        public static string Format(string phoneNumber)
        {
            return phoneNumber != null ? Regex.Replace(phoneNumber, "[^0-9]", string.Empty) : null;
        }
    }
}