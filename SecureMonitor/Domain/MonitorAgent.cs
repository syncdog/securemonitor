﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class MonitorAgent
    {
        public virtual long MonitorAgentID { get; set; }
        public virtual int MonitorAgentType { get; set; }
        public virtual string MonitorAgentName { get; set; }
        public virtual string RelayServerIPAddress { get; set; }
        public virtual int RelayServerPort { get; set; }
        public virtual string ProvisionEmailAddress { get; set; }
        public virtual string ProvisionPIN { get; set; }
        public virtual DateTime LastAlertTime { get; set; }
        public virtual bool RawSocketTest { get; set; }
        public virtual bool DefaultPageTest { get; set; }
        public virtual bool ProvisionTest { get; set; }


    }
}
