﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class MonitorConactGroup
    {
        public virtual long MontitorConactGroupID { get; set; }
        public virtual string MonitorGroupName { get; set; }
        public virtual int AlertType { get; set; }
        public virtual long AlertInterval { get; set; }
    }
}
