﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class MonitorResult
    {
        public virtual long MonitorResultID { get; set; }
        public virtual long MonitorAgentID { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual string ResultMessage { get; set; }
        public virtual long ResultStatus { get; set; }
        public virtual bool AlertSent { get; set; }
    }
}
