﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class MonitorAgentConactGroup
    {

        public virtual long MonitorAgentConactGroupID { get; set; }
        public virtual MonitorConactGroup MonitorConactGroup { get;  set; }
        public virtual MonitorAgent MonitorAgent { get; set; }

    }
}
