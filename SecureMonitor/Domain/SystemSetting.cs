﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class SystemSetting
    {
        public virtual long ID { get; set; }
        public virtual string SystemProperty { get; set; }
        public virtual string SystemValue { get; set; }
    }
}
