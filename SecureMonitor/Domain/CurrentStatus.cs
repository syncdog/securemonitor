﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Domain
{
    public class CurrentStatus
    {
        public virtual long CurrentStatusID { get; set; }
        public virtual DateTime LastRunTime { get; set; }
        public virtual long Server { get; set; }
        public virtual int Status { get; set; }
        public virtual long Agent { get; set; }
        public virtual DateTime StatusTime { get; set; }
    }
}
