﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMonitor.Enums
{
    public enum AgentEnum
    {
        Provision = 1,
        Notification = 2
    }
}
