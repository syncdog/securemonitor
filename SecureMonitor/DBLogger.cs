﻿using System;
using System.Linq;
using SecureMonitor.Domain;

namespace SecureMonitor
{
    public static class DBLogger
    {



        public static void LogResult(string message, long resultStatus, bool alertSent, long monitorAgentId)
        {

            using (var ctx = new SecureMonitorContext())
            {
                var entry = new MonitorResult()
                {
                    DateCreated = DateTime.UtcNow,
                    MonitorAgentID = monitorAgentId,
                    ResultMessage = message,
                    AlertSent = alertSent,
                    ResultStatus = resultStatus
                };

//                ctx.MonitorResults.Add(entry);
//                ctx.SaveChanges();
            }


        }

        public static void UpdateCurrentStatus(long server, long agent, int status, DateTime statusStamp)
        {
            return;
            using (var ctx = new SecureMonitorContext())
            {
                var statusEntry = ctx.CurrentStatus.FirstOrDefault(s => s.Agent == agent && s.Server == server);

                if (statusEntry != null)
                {
                    statusEntry.Status = status;
                    statusEntry.StatusTime = statusStamp;
                    statusEntry.LastRunTime = DateTime.UtcNow;

                    ctx.Entry(statusEntry).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                else
                {
                    statusEntry = new CurrentStatus()
                    {
                        Agent = agent,
                        Server = server,
                        Status = status,
                        StatusTime = statusStamp,
                        LastRunTime = DateTime.UtcNow
                    };
                    ctx.CurrentStatus.Add(statusEntry);
                    ctx.SaveChanges();
                }




            }

        }

    }
}
