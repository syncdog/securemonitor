﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SecureMonitor.RelayService;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Net;
using System.Net.Sockets;
using NLog;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using TransportCore.Repositories;
using NHibernate.Mapping;
using Array = System.Array;
using System.ServiceModel.Channels;

namespace SecureMonitor
{
    class Program
    {
        private const int PUBLIC_KEY_BYTE_LENGTH = 72;
        private const int HMAC_BYTE_LENGTH = 32;
        private const int KEY_HMAC_COMBINED_BYTE_LENGTH = 104;
        private const int TEST_INTERVAL = 600000;
        private static DateTime LastNotificationTime;


        private static string RelayServerUrl = "localhost";
        private static int RelayServerPort = 51491;

        private static List<string> phoneNumberList;
        private static bool SendAlertMsg = false;
        private const int SECOND_TRY_INTERVAL = 120000;

        private static string Environment = "Azure";
        private static long Server = 1;
        private static long Agent = 1;
        private static string EmailAddress = "eric@syncdog.com";
        private static string PIN = "982456";

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static SynchronizedCollection<string> ProvisionedDevices = new SynchronizedCollection<string>();


        static void Main(string[] args)
        {

            EmailAddress = ConfigurationManager.AppSettings.Get("UserName");
            PIN = ConfigurationManager.AppSettings.Get("PIN");
            var serverStr = ConfigurationManager.AppSettings.Get("Server");
            var agentStr = ConfigurationManager.AppSettings.Get("Agent");
            Environment = ConfigurationManager.AppSettings.Get("Environment");
            RelayServerUrl = ConfigurationManager.AppSettings.Get("RelayURL");
            var relayPortStr = ConfigurationManager.AppSettings.Get("RelayPort");
            var monitorNotifications = ConfigurationManager.AppSettings.Get("MonitorNotifications").ToLower().Equals("true");
            var conStrSentinelSecure = ConfigurationManager.AppSettings.Get("SentinelSecureDB");
            phoneNumberList = new List<string>(ConfigurationManager.AppSettings["NotificationList"].Split(';'));


            if (!int.TryParse(relayPortStr, out RelayServerPort))
            {
                RelayServerPort = 80;
            }
            if (!long.TryParse(serverStr, out Server))
            {
                Server = 1;
            }
            if (!long.TryParse(agentStr, out Agent))
            {
                Agent = 80;
            }

            /*
            var processor = new DeviceProvisionProcessor(1);
            processor.RunProvisionTest();
            */

            var taskList = new List<Task>();
            for (var i = 1; i <= 1; i++)
            {
                Console.WriteLine("Invoke RunProvisionTest...iteration " + i);
                var index = i;
                /*
                var processor = new DeviceProvisionProcessor(index);
                processor.RunProvisionTest();
                */
                taskList.Add(Task.Run(() =>
                {
                    var processor = new DeviceProvisionProcessor(index);
                    processor.RunProvisionTest();
                }));
                Thread.Sleep(500);
            }

            Task.WaitAll(taskList.ToArray());
            Console.WriteLine("Tasks finished..." + ProvisionedDevices.Count + " devices created");

            var deviceRepository = new DeviceRepository();
            foreach (var provisionedDevice in ProvisionedDevices)
            {
                deviceRepository.DeleteDevice(long.Parse(provisionedDevice), true);
            }
            Console.WriteLine("{0} devices deleted...", ProvisionedDevices.Count);

            Console.WriteLine("Done...Hit return to exit");
            Console.ReadLine();
            return;


            /*
            Logger.Info("Startup for {0}", Environment);

            LastNotificationTime = DateTime.UtcNow.AddMinutes(-65);

            var lastNotificationMonitor = new LastNotificationMonitor(conStrSentinelSecure, Environment, phoneNumberList);

            while (true)
            {

                if (monitorNotifications)
                {
                    lastNotificationMonitor.RunLastNotificationMonitor();
                }


                SendAlertMsg = true;
                if (RunConnectTest())
                {

                    if (RunDefaultPageTest())
                    {
                        SendAlertMsg = false;
                        if (!RunProvisionTest(0))
                        {
                            Thread.Sleep(SECOND_TRY_INTERVAL);
                            SendAlertMsg = true;
                            RunProvisionTest(0);
                            // Console.WriteLine("Provision test successful");
                        }
                    }
                }

                // Use TimeSpan constructor to specify:
                // ... Days, hours, minutes, seconds, milliseconds.

                // TimeSpan span = new TimeSpan(0, 0, 10, 0, 0);
                // Task.Delay(span);
                Thread.Sleep(TEST_INTERVAL);
            }
        */
        }


        private static bool RunDefaultPageTest()
        {
            var retVal = false;

            using (WebClient client = new WebClient())
            {
                try
                {
                    string url = String.Format("http://{0}:{1}/RelayService.svc", RelayServerUrl, RelayServerPort);
                    string htmlCode = client.DownloadString(url); // Relay service

                    if (htmlCode.Length < 1)
                    {
                        Console.WriteLine("Relay server default page empty");
                        if (SendAlert())
                        {
                            var alert = new SmsAlert();
                            var errorMsg = String.Format("{2}: Request for http://{0}:{1}/RelayService.svc failed}", RelayServerUrl, RelayServerPort, Environment);

                            alert.SendSmsAlert(errorMsg, phoneNumberList);

                            DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                        }
                        retVal = false;
                    }
                    else
                    {
                        retVal = true;
                    }
                }
                catch (Exception ex)
                {
                    if (SendAlert())
                    {
                        Logger.Error("Connection to relay server: {0} on port: {1} failed", RelayServerUrl, RelayServerPort);
                        var alert = new SmsAlert();
                        var errorMsg = String.Format("{2}: Connection to relay server: {0} on port: {1} failed", RelayServerUrl, RelayServerPort, Environment);
                        DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                }

            }

            return retVal;
        }

        private static bool RunConnectTest()
        {
            var retVal = false;

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.   
                //  IPHostEntry ipHostInfo = Dns.GetHostEntry(RelayServerUrl);
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("relay.secure.systems");
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                //IPEndPoint remoteEP = new IPEndPoint(ipAddress, 80);

                //  IPHostEntry ipHostInfo = Dns.GetHostEntry(RelayServerUrl);
                //  IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPAddress ipAddress; // = IPAddress.Parse(RelayServerUrl);

                if (!IPAddress.TryParse(RelayServerUrl, out ipAddress))
                {
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(RelayServerUrl);
                    ipAddress = ipHostInfo.AddressList[0];
                }

                IPEndPoint remoteEP = new IPEndPoint(ipAddress, RelayServerPort);

                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);

                    //    Console.WriteLine("Socket connected to {0}",
                    sender.RemoteEndPoint.ToString();


                    // Release the socket.  
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                    retVal = true;

                }
                catch (ArgumentNullException ane)
                {
                    Logger.Error("ArgumentNullException : {0}", ane.ToString());
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Logger.Error("SocketException : {0}", se.ToString());
                    var errorMsg = String.Format("{2}: Connection to relay server: {0} on port: {1} failed", RelayServerUrl, RelayServerPort, Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();

                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                }
                catch (Exception e)
                {
                    Logger.Error("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                var errorMsg = String.Format("{2}: Connection to relay server: {0} on port: {1} failed", RelayServerUrl, RelayServerPort, Environment);

                DBLogger.LogResult(errorMsg, 1, false, 1);
            }
            return retVal;
        }

        class DeviceProvisionProcessor
        {
            private int index;

            public DeviceProvisionProcessor(int index)
            {
                this.index = index;
            }

            public bool RunProvisionTest()
            {
                var retVal = false;

                var protocol = RelayServerPort == 443 ? "https" : "http";
                var portString = RelayServerPort == 443 ? "" : ":" + RelayServerPort;
                if (RelayServerPort == 80)
                {
                    portString = "";
                }
                Console.WriteLine("Starting provision test..." + index);
                string url = String.Format("{2}://{0}{1}/RelayService.svc", RelayServerUrl, portString, protocol);
                //RelayServiceClient client = new RelayServiceClient("HttpsBinding_IRelayService", url);
                RelayServiceClient client = new RelayServiceClient("BasicHttpBinding_IRelayService", url);

                List<object> keyList = GenerateECDHKeys();
                byte[] clientPubKeyBlob = new byte[72];
                int listCounter = 0;
                CngKey serverKey = null;

                foreach (object key in keyList)
                {
                    if (listCounter == 0)
                        serverKey = (CngKey)key;
                    if (listCounter == 1)
                        clientPubKeyBlob = (byte[])key;
                    listCounter++;
                }


                Console.WriteLine("Starting GetProvisionKeyExchange..." + index);
                //var provisionKeyExchangeStr = GetProvisionKeyExchange(clientPubKeyBlob);

                //  Console.WriteLine("Send Key: {0}", provisionKeyExchangeStr);

                //var encodedPubKey = Encoding.ASCII.GetBytes(provisionKeyExchangeStr);
                
                byte[] result = new byte[PUBLIC_KEY_BYTE_LENGTH];

                try
                {

                    using (var scope = new OperationContextScope(client.InnerChannel))
                    {

                        /*
                        HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                        requestMessage.Headers["Content-Type"] = "application/soap+xml; charset=utf-8";
                        OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;
                        */
                        
                        result = client.GetProvisionKeyExchange(clientPubKeyBlob);
                        Console.WriteLine("Finished GetProvisionKeyExchange..." + index);
                    }

                }
                catch (EndpointNotFoundException ex)
                {
                    // Could not connect to relay endpoint
                    Logger.Error("Error: {0}", ex.Message);
                    var errorMsg = String.Format("{0}: GetProvisionKeyExchange connection failed to relay", Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();

                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(1, 1, 2, DateTime.UtcNow);
                    client.Close();
                    return false;

                }
                catch (FaultException ex)
                {
                    // Relay endpoint was reached but RelayQueue did not respond
                    Logger.Error(ex, "Error: {0}", ex.Message);
                    var errorMsg = String.Format("{0}: GetProvisionKeyExchange failed", Environment);

                    if (SendAlert())
                    {
                        var alert = new SmsAlert();

                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);

                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }
                catch (TimeoutException ex)
                {
                    //Contacted Relay and delivered the packet but no response from TQP
                    Logger.Error(ex, "Error: {0}", ex.Message);
                    var errorMsg = String.Format("{0}: GetProvisionKeyExchange failed timeout exception", Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }


                if (result == null)
                {


                    var errorMsg = String.Format("{0}: GetProvisionKeyExchange failed with null value", Environment);
                    Logger.Error(errorMsg);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }

                try
                {
                    Console.WriteLine("Starting EncryptDataToSend..." + index);
                    var encData = EncryptDataToSend(GetProvisionPackage(), result, serverKey);

                    byte[] dataToSend = new byte[encData.Length + KEY_HMAC_COMBINED_BYTE_LENGTH];

                    Array.Copy(clientPubKeyBlob, dataToSend, PUBLIC_KEY_BYTE_LENGTH);

                    byte[] HMACValue = ComputeHMAC(serverKey, encData, result);

                    Array.Copy(HMACValue, 0, dataToSend, PUBLIC_KEY_BYTE_LENGTH, HMAC_BYTE_LENGTH);

                    Array.Copy(encData, 0, dataToSend, KEY_HMAC_COMBINED_BYTE_LENGTH, encData.Length);

                    // Console.WriteLine("DataLen = {0}, total len = {1}", encData.Length, dataToSend.Length);


                    Console.WriteLine("Starting ProvisionDevice..." + index);
                    var provisionData = client.ProvisionDevice(dataToSend);
                    Console.WriteLine("Finished ProvisionDevice..." + index);

                    if (provisionData == null)
                    {
                        //Step 2 failed -- public keys were exchanged but no response to provision device

                        var errorMsg = String.Format("{0}: ProvisionDevice failed null response", Environment);
                        Logger.Error(errorMsg);
                        if (SendAlert())
                        {
                            var alert = new SmsAlert();
                            DBLogger.LogResult(errorMsg, 1, true, 1);
                            alert.SendSmsAlert(errorMsg, phoneNumberList);
                        }
                        else
                        {
                            DBLogger.LogResult(errorMsg, 1, false, 1);
                        }
                        DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                        client.Close();
                        return false;
                    }


                    string provisionRequestXml = Decrypt(result, serverKey, provisionData);

                    //     Console.WriteLine(provisionRequestXml);

                    XElement root = XElement.Parse(provisionRequestXml);


                    IEnumerable<string> errorMessage = GetProvisionElementValue(root, "ErrorMessage");

                    if (errorMessage != null && errorMessage.Any())
                    {
                        // TQP Responsed to device provision but there was an error with the provision
                        Logger.Error("Error: {0}", errorMessage.FirstOrDefault());
                        //if (SendAlert())
                        //{
                        //    var alert = new SmsAlert();
                        //    alert.SendSmsAlert(errorMessage.FirstOrDefault(), "12392936387");
                        //}
                        var errorMsg = String.Format("{1}: ProvisionDevice failed: {0}", errorMessage.FirstOrDefault(), Environment);
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                        DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                        client.Close();
                        Console.WriteLine("Provision failed...");
                        return false;
                    }
                    else
                    {
                        //Successful provision

                        var currentTime = DateTime.UtcNow;

                        var deviceId = GetProvisionElementValue(root, "DeviceID").FirstOrDefault();
                        if (!string.IsNullOrEmpty(deviceId))
                        {
                            ProvisionedDevices.Add(deviceId);
                            Console.WriteLine("Provision Successful...DeviceId=" + deviceId + ", index=" + index);
                        }
                        else
                        {
                            Console.WriteLine("Missing deviceId from provision response");
                        }

                        Console.WriteLine("Successful provision deviceId={0} at {1}....{2}", deviceId, currentTime.ToString(), index);
                        var msg = String.Format("{1}: Successful provision at {0}", currentTime.ToString(), Environment);
                        DBLogger.LogResult(msg, 1, false, 1);
                        DBLogger.UpdateCurrentStatus(Server, Agent, 1, DateTime.UtcNow);

                        var nTime = LastNotificationTime.AddMinutes(60);

                        int compResult = DateTime.Compare(nTime, DateTime.UtcNow);

                        if (compResult > 0)
                        {
                            // send all clear
                            var alert = new SmsAlert(); var errorMsg = String.Format("{0}: Provision successful", Environment);
                            alert.SendSmsAlert(errorMsg, phoneNumberList);

                        }
                        //reset last notification time
                        LastNotificationTime = DateTime.UtcNow.AddMinutes(-65);


                        retVal = true;
                        //IEnumerable<string> nextMessageId = GetProvisionElementValue(root, "NextMessageID");
                        //IEnumerable<string> certificate = GetProvisionElementValue(root, "Certificate");
                        //IEnumerable<string> deviceID = GetProvisionElementValue(root, "DeviceID");
                        //IEnumerable<string> policies = GetProvisionElementValue(root, "Policies");

                        //Console.WriteLine("NextMessageID: {0}", nextMessageId.FirstOrDefault());
                        //Console.WriteLine("Certificate: {0}", certificate.FirstOrDefault());
                        //Console.WriteLine("DeviceID: {0}", deviceID.FirstOrDefault());
                        //Console.WriteLine("Policies: {0}", policies.FirstOrDefault());

                        //XDocument doc = XDocument.Parse(provisionRequestXml); //or XDocument.Load(path)
                        //string jsonText = JsonConvert.SerializeXNode(doc);
                        //dynamic dyn = JsonConvert.DeserializeObject<ExpandoObject>(jsonText);
                    }
                }
                catch (TimeoutException ex)
                {
                    //Step 2 failed -- public keys were exchanged but no response to provision device
                    Logger.Error(ex, "Error: {0} \n {1}", ex.Message, ex.ToString());
                    var errorMsg = String.Format("{0}: ProvisionDevice failed timeout error", Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }

                catch (FaultException ex)
                {
                    Logger.Error(ex, "Error: {0} \n {1}", ex.Message, ex.ToString());
                    var errorMsg = String.Format("{0}: ProvisionDevice failed", Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "Error: {0} \n {1}", ex.Message, ex.ToString());
                    var errorMsg = String.Format("{0}: ProvisionDevice failed", Environment);
                    if (SendAlert())
                    {
                        var alert = new SmsAlert();
                        DBLogger.LogResult(errorMsg, 1, true, 1);
                        alert.SendSmsAlert(errorMsg, phoneNumberList);
                    }
                    else
                    {
                        DBLogger.LogResult(errorMsg, 1, false, 1);
                    }
                    DBLogger.UpdateCurrentStatus(Server, Agent, 2, DateTime.UtcNow);
                    client.Close();
                    return false;
                }


                client.Close();

                return retVal;
            }

            private byte[] ComputeHMAC(CngKey key, byte[] EncryptedProvisionRequest, byte[] DevicePublicKey)
            {
                byte[] hashValue = null;
                byte[] symmKey;


                using (var serverAlgorithm = new ECDiffieHellmanCng(key))
                using (CngKey devicePubKey = CngKey.Import(DevicePublicKey,
                      CngKeyBlobFormat.EccPublicBlob))
                {
                    symmKey = serverAlgorithm.DeriveKeyMaterial(devicePubKey);
                }

                using (HMACSHA256 hmac = new HMACSHA256(symmKey))
                {
                    hashValue = hmac.ComputeHash(EncryptedProvisionRequest);
                }



                return hashValue;


            }
            private bool ValidateHMAC(CngKey key, byte[] EncryptedProvisionRequest, byte[] messageHMAC, byte[] DevicePublicKey)
            {
                byte[] hashValue = null;
                byte[] symmKey;


                using (var serverAlgorithm = new ECDiffieHellmanCng(key))
                using (CngKey devicePubKey = CngKey.Import(DevicePublicKey,
                      CngKeyBlobFormat.EccPublicBlob))
                {
                    symmKey = serverAlgorithm.DeriveKeyMaterial(devicePubKey);
                }

                using (HMACSHA256 hmac = new HMACSHA256(symmKey))
                {
                    hashValue = hmac.ComputeHash(EncryptedProvisionRequest);
                }

                ByteArrayComparer bc = new ByteArrayComparer();

                return bc.Equals(messageHMAC, hashValue);
            }

            private string Decrypt(byte[] DevicePublicKey, CngKey SharedSecret, byte[] encryptedData)
            {
                //  Console.WriteLine(String.Format("server receives encrypted data: Length={0}", encryptedData.Length));
                byte[] rawData = null;
                string outputXml;

                var aes = new AesCryptoServiceProvider();
                int nBytes = aes.BlockSize >> 3;
                byte[] iv = new byte[nBytes];
                for (int i = 0; i < iv.Length; i++)
                    iv[i] = encryptedData[i];

                using (var serverAlgorithm = new ECDiffieHellmanCng(SharedSecret))
                using (CngKey devicePubKey = CngKey.Import(DevicePublicKey,
                      CngKeyBlobFormat.EccPublicBlob))
                {
                    byte[] symmKey = serverAlgorithm.DeriveKeyMaterial(devicePubKey);
                    //     Console.WriteLine(String.Format("server creates this symmetric key with " +
                    //           "devices public key information: {0}",
                    //          Convert.ToBase64String(symmKey)));

                    aes.Key = symmKey;
                    aes.IV = iv;

                    using (ICryptoTransform decryptor = aes.CreateDecryptor())
                    using (MemoryStream ms = new MemoryStream())
                    {
                        var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write);
                        cs.Write(encryptedData, nBytes, encryptedData.Length - nBytes);
                        cs.Close();
                        rawData = ms.ToArray();

                        //             Console.WriteLine( String.Format("server decrypts message to: {0}",
                        //                   Encoding.UTF8.GetString(rawData)));
                        outputXml = Encoding.UTF8.GetString(rawData);
                    }
                    aes.Clear();
                }

                return outputXml;
            }

            private byte[] EncryptDataToSend(string message, byte[] devicePubKeyBlob, CngKey SharedSecret)
            {
                //  Console.WriteLine("device sends message: {0}", message);
                byte[] rawData = Encoding.UTF8.GetBytes(message);
                byte[] encryptedData = null;

                using (var deviceAlgorithm = new ECDiffieHellmanCng(SharedSecret))
                using (CngKey serverPubKey = CngKey.Import(devicePubKeyBlob,
                     CngKeyBlobFormat.EccPublicBlob))
                {
                    byte[] symmKey = deviceAlgorithm.DeriveKeyMaterial(serverPubKey);
                    //    Console.WriteLine("device creates this symmetric key with " +
                    //          "servers public key information: {0}",
                    ///          Convert.ToBase64String(symmKey));


                    using (var aes = new AesCryptoServiceProvider())
                    {
                        aes.Key = symmKey;
                        aes.GenerateIV();
                        using (ICryptoTransform encryptor = aes.CreateEncryptor())
                        using (MemoryStream ms = new MemoryStream())
                        {
                            // create CryptoStream and encrypt data to send 
                            var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);

                            // write initialization vector not encrypted 
                            ms.Write(aes.IV, 0, aes.IV.Length);
                            cs.Write(rawData, 0, rawData.Length);
                            cs.Close();
                            encryptedData = ms.ToArray();
                        }
                        aes.Clear();
                    }
                }

                //   Console.WriteLine("device: message is encrypted: {0}",
                //          Convert.ToBase64String(encryptedData)); ;
                //  Console.WriteLine();


                return encryptedData;
            }

            private List<object> GenerateECDHKeys()
            {
                ProvisionKeyExchange pe = new ProvisionKeyExchange();
                return pe.CreateECDHKey();
            }


            private CngKey CreateECDHKey()
            {

                var clientKey = CngKey.Create(CngAlgorithm.ECDiffieHellmanP256, null, new CngKeyCreationParameters { ExportPolicy = CngExportPolicies.AllowPlaintextExport });


                return clientKey;

            }

            private string GetProvisionKeyExchange(byte[] pubKey)
            {
                var data = new XElement("GetProvisionKeyExchange",
                    new XElement("DeviceKey",
                        //     Convert.ToBase64String(pubKey))
                        pubKey)
                    );


                return data.ToString();
            }

            private string GetProvisionPackage()
            {

                var provisionData = new XElement("Provision",
                        new XElement("EmailAddress", EmailAddress),
                        new XElement("PIN", PIN),
                          new XElement("DeviceID", "ae53b6c09d4a47bd"),
                         new XElement("DeviceOS", "windows"),
                         new XElement("DeviceOSVersion", "9"),
                         new XElement("DeviceType", "desktop"),
                         new XElement("DeviceToken", ""),
                         new XElement("Carrier", "Unknown"),
                         new XElement("Manufacturer", "SecureSystems"),
                         new XElement("Model", "ONEPLUS A6013"),
                         new XElement("Build", "441"),
                         new XElement("DeviceName", "SSMonitor_" + Guid.NewGuid())
                    );

                /*
                var provisionData = new XElement("Provision",
                        new XElement("EmailAddress", EmailAddress),
                        new XElement("PIN", PIN),
                          new XElement("DeviceID", "ae53b6c09d4a47bd"),
                         new XElement("DeviceOS", "android"),
                         new XElement("DeviceOSVersion", "9"),
                         new XElement("DeviceType", "OnePlus6T"),
                         new XElement("DeviceToken", ""),
                         new XElement("Carrier", "Unknown"),
                         new XElement("Manufacturer", "SecureSystems"),
                         new XElement("Model", "ONEPLUS A6013"),
                         new XElement("Build", "441"),
                         new XElement("DeviceName", "SSMonitor_" + Guid.NewGuid())
                    );

                */
                //       Console.WriteLine(provisonData);
                return provisionData.ToString();
            }


            private IEnumerable<string> GetProvisionElementValue(XElement root, string ElementName)
            {
                IEnumerable<string> value =
                    from e in root.Descendants(ElementName)
                    select (string)e;
                return value;
            }

        }


        private static bool SendAlert()
        {
            var nTime = LastNotificationTime.AddMinutes(60);

            int result = DateTime.Compare(nTime, DateTime.UtcNow);

            if (result < 0 && SendAlertMsg)
            {
                LastNotificationTime = DateTime.UtcNow;
                return true;
            }

            return false;
        }
    }
}

