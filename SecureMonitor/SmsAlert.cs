﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using SecureMonitor.Domain;
using NLog;

namespace SecureMonitor
{
   public class SmsAlert
    {

        private const string Id = "sms_accound_sid";
        private const string Password = "sms_auth_token";
        private const string SenderId = "sms_sender_id";



       private string _accountSid;
       private string _authToken;
       private string _senderId;
       private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SmsAlert()
        {
            GetSmsSettings();
        }

        public bool SendSmsAlert(string message, List<string> phoneNumberList)
        {

            if (AreSmsSettingsValid())
            {

                foreach (var phoneNumber in phoneNumberList)
                {

                    try
                    {
                        var recipient = "+" + PhoneHelper.Format(phoneNumber);

                        TwilioClient.Init(_accountSid, _authToken);

                        var sentMessage = MessageResource.Create(
                            body: message,
                            from: new Twilio.Types.PhoneNumber(_senderId),
                            to: new Twilio.Types.PhoneNumber(recipient)
                        );
                        Console.WriteLine("SMS Alert sent to {0}", phoneNumber);
                    }
                   
                    catch(Exception ex)
                    {
                        Console.WriteLine("Error: {0}", ex.Message);
                    }

                }

                

                return true;
            }

            return false;
        }

        private bool GetSmsSettings()
        {

      //      using (var ctx = new SecureMonitorContext())
      //      {
                //  var accountSid = ctx.SystemSettings.Where(s => s.SystemProperty.Equals("sms_accound_sid")).FirstOrDefault<SystemSetting>();
                var accountSid = "AC8bdfef3fd9725383288e8f3f7eb7de5e";

                if (accountSid != null && !String.IsNullOrEmpty(accountSid))
                {
                    var authToken = "82432b6116a5cc5937dca3c2935cdc68"; //ctx.SystemSettings.Where(s => s.SystemProperty.Equals("sms_auth_token")).FirstOrDefault<SystemSetting>();
                    var senderId = "13237962364"; // ctx.SystemSettings.Where(s => s.SystemProperty.Equals("sms_sender_id")).FirstOrDefault<SystemSetting>();

                    if (authToken != null && senderId != null)
                    {
                        _accountSid = accountSid; //.SystemValue;
                        _authToken = authToken; //.SystemValue;
                        _senderId = senderId; //.SystemValue;
                        return true;
                    }

                }

       //     }

            return false;
        }

        private bool AreSmsSettingsValid()
        {
            var retval = true;

            if (String.IsNullOrEmpty(_accountSid))
            {
                retval = false;
            }

            if (String.IsNullOrEmpty(_authToken))
            {
                retval = false;
            }

            if (String.IsNullOrEmpty(_senderId))
            {
                retval = false;
            }

            return retval;
        }


    }
}
